import * as functions from 'firebase-functions';
import { firebaseAdminAuth } from '../../db';
import { IAuth, IAuthFirebase } from './auth';

export function parseLoginData(signature: string, address: string, challenge: string): IAuth {
  return {
    challenge,
    address,
    signature,
  } as IAuth;
}

export function getAuth(context: functions.https.CallableContext): IAuthFirebase {
  // Checking that the user is authenticated.
  if (!context.auth) {
    // Throwing an HttpsError so that the client gets the error details.
    throw new functions.https.HttpsError('failed-precondition', 'The function must be called '
            + 'while authenticated.');
  }

  const userId = context.auth.uid;
  const { token } = context.auth;
  return {
    id: userId,
    provider: token.firebase.sign_in_provider,
  };
}

export async function createJsonWebToken(userId: string): Promise<string> {
  const additionalClaims = {
    admin: false,
  };
  const token = await firebaseAdminAuth().createCustomToken(userId, additionalClaims);
  return token;
}
