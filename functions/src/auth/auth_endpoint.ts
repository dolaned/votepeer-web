import * as functions from 'firebase-functions';
import { generateChallenge, verifyAuthSignature, verifySignature } from '../challenge';
import { getGetParam } from '../util';
import * as User from '../user/user_endpoint';
import * as Auth from './model/auth_provider_firebase';
import { InvalidParamError, SignatureError, ServerError } from '../error';

export async function requestChallenge(
  data: any,
  context: functions.https.CallableContext,
): Promise<string> {
  const authUser = Auth.getAuth(context);
  const challenge = generateChallenge();
  await User.storeChallenge(challenge, authUser);

  return challenge;
}

export async function verifySignedChallenge(data: any, context: functions.https.CallableContext) {
  Auth.getAuth(context);
  const { publicKey } = data;
  const { signature } = data;
  const { challenge } = data;
  const loginData = Auth.parseLoginData(publicKey, signature, challenge);

  if (verifyAuthSignature(loginData)) {
    const jsonWebToken = await Auth.createJsonWebToken(publicKey);
    return {
      jsonWebToken,
    };
  }
  throw new functions.https.HttpsError(
    'invalid-argument',
    'The function must be called with a valid signature',
  );
}

export async function identify(req: any, res: any) {
  const operation = getGetParam<string>(req, 'op');
  if (operation !== 'login') {
    throw new InvalidParamError('op', "Expected value 'login'");
  }
  const cookie = getGetParam<string>(req, 'cookie');
  const addr = getGetParam<string>(req, 'addr');
  const sig = getGetParam<string>(req, 'sig');

  let challenge: string = '';
  try {
    challenge = await User.getChallenge(cookie);
  } catch (e) {
    throw new InvalidParamError('cookie',
      'No challenge exists for cookie.');
  }

  try {
    if (!verifySignature(challenge, addr, sig)) {
      throw new SignatureError('Signature does not verify');
    }
  } catch (e) {
    throw new SignatureError(`Signature verification error: ${e}`);
  }

  try {
    const jsonWebToken = await Auth.createJsonWebToken(addr);
    await User.storeAuthToken(cookie, jsonWebToken);
    return JSON.stringify({ token: jsonWebToken });
  } catch (e) {
    throw new ServerError(`Failed to create authentication token: ${e}`);
  }
}
