/**
 * Something happened that was probably the users fault.
 */
export default class UserError extends Error { }
