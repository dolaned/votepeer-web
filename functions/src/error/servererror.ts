/**
 * Something (weird) happened that was probably not the users fault.
 */
export default class ServerError extends Error { }
