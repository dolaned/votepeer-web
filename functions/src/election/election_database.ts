import * as functions from 'firebase-functions';
import admin from 'firebase-admin';
import { firebaseAdminFirestore } from '../db';
import {
  Election,
  CONTRACT_TYPE_TWO_OPTION_VOTE,
  VISIBILITY_PARTICIPANTS,
} from './model/election';
import { DoesNotExistError, ServerError } from '../error';
import { constants } from '../constants';

export function toElection(
  electionSnap: functions.firestore.DocumentSnapshot,
): Election {
  if (electionSnap === undefined) {
    throw new DoesNotExistError('Election does not exist');
  }
  const adminAddress: string = electionSnap.get('adminAddress');
  let options: string[] = electionSnap.get('option');
  const description: string = electionSnap.get('description');
  const participantAddresses: string[] = electionSnap.get('participantAddresses');
  const endHeight: number = electionSnap.get('endHeight');
  const salt: string = electionSnap.get('salt');
  let tags: string[] = electionSnap.get('tags');
  if (tags === undefined) {
    tags = [];
  }

  let contractType = electionSnap.get('contractType');

  if (options === undefined || options.length === 0) {
    // Election created before X
    options = [
        electionSnap.get('optionA') as string,
        electionSnap.get('optionB') as string,
    ];
  }

  if (contractType === undefined) {
    // Elections created before Aug 2021
    contractType = CONTRACT_TYPE_TWO_OPTION_VOTE;
  }

  let visibility = electionSnap.get('visibility');
  if (visibility === undefined) {
    // Elections created before Sep 2021
    visibility = VISIBILITY_PARTICIPANTS;
  }
  const createdAtTimestamp: admin.firestore.Timestamp = electionSnap.get('createdAt');
  const createdAt: number = electionSnap.get('createdAt');
  let beginHeight = electionSnap.get('beginHeight');

  if (beginHeight === undefined) {
    // Not supported by all election types.
    beginHeight = -1;
  }

  return new Election(
    electionSnap.id,
    adminAddress,
    description,
    participantAddresses,
    salt,
    endHeight,
    contractType,
    visibility,
    options,
    tags,
    createdAtTimestamp,
    createdAt,
    beginHeight,
  );
}

export async function getElection(electionId: string): Promise<Election> {
  const electionDocument = await firebaseAdminFirestore()
    .collection(constants.election.path)
    .doc(electionId)
    .get();
  return toElection(electionDocument);
}

export async function deleteElection(electionId: string): Promise<void> {
  await firebaseAdminFirestore()
    .collection(constants.election.path)
    .doc(electionId)
    .delete();
}

export interface QueryCallback {
  (query: any): any
}

export async function listElections(
  limit: number,
  cursor: string | null,
  filterTags: string[] | null,
  extraQueryConstraint: null | QueryCallback,
): Promise<[Array<Election>, string | null]> {
  const electionRef = await firebaseAdminFirestore().collection(constants.election.path);

  let query = electionRef
    .orderBy(constants.election.fields.createdAt, 'desc')
    .limit(limit);

  if (extraQueryConstraint !== null) {
    query = extraQueryConstraint(query);
  }

  if (cursor !== null) {
    const cursorDecoded = Election.decodeCreatedAt(cursor);
    query = query.startAfter(cursorDecoded);
  }

  if (filterTags !== null) {
    // TODO: Support more than 1 tag. Firestore does not allowe multiple where
    // clauses for 'array-contains'.
    if (filterTags.length > 1) {
      throw new ServerError('Filter on multiple tags is not implemented.');
    }
    for (const t of filterTags) {
      query = query.where(constants.election.fields.tags,
        'array-contains',
        t);
    }
  }

  const snapshot = await query.get();
  const elections: Election[] = [];
  snapshot.forEach((s: functions.firestore.DocumentSnapshot) => {
    const e = toElection(s);
    elections.push(e);
  });
  const nextCursor = elections.length === 0
    ? null
    : elections[elections.length - 1].encodeCreatedAt();

  return [elections, nextCursor];
}

export async function deleteElectionsWithTag(tag: string): Promise<any> {
  let elections: Array<any> = [];
  do {
    [elections] = await listElections(100, null, [tag], null);
    await Promise.all(elections.map(async (e) => deleteElection(e.id)));
  } while (elections.length !== 0);
}

export async function setElectionOwner(electionID: string, newOwner: string): Promise<any> {
  return firebaseAdminFirestore()
    .collection(constants.election.path)
    .doc(electionID)
    .update({
      [constants.election.fields.adminAddress]: newOwner,
    });
}
