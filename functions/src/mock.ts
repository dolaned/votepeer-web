import bchaddrjs from 'bchaddrjs';

const bitcore = require('bitcore-lib');

export function getMockPrivKey(): Buffer {
  return bitcore.crypto.Hash.sha256(Buffer.from('Det bli itj no fæst uten pategoniavest'));
}

/// Creates private key bitcore object
function getMockPrivKeyBitcore() {
  return bitcore.PrivateKey(
    bitcore.crypto.BN.fromBuffer(getMockPrivKey()),
  );
}

export function getMockPubkeyBitcore() {
  return getMockPrivKeyBitcore().toPublicKey();
}

export function getMockPubKey(): Buffer {
  return getMockPubkeyBitcore().toBuffer();
}

export function getMockPKH() {
  return bitcore.crypto.Hash.sha256ripemd160(getMockPubKey());
}

export function getMockLegacyAddress(): string {
  return bitcore.Address.fromPublicKey(getMockPubkeyBitcore(), 'mainnet').toString();
}

export function getMockAddress(): string {
  return bchaddrjs.toCashAddress(getMockLegacyAddress());
}

export function getMockChallenge() {
  return 'Bli itj nå fart uten bart';
}

export function getMockSignature(challenge: string): string {
  return new bitcore.Message(challenge).sign(getMockPrivKeyBitcore());
}
