import { getReasonPhrase } from 'http-status-codes';
import { logger } from './db';
import { MissingParamError } from './error';

const crypto = require('crypto');

export function getRandomInt(min: number, max: number) {
  const ceilMin = Math.ceil(min);
  const ceilMax = Math.floor(max);
  // The maximum is exclusive and the minimum is inclusive:
  return Math.floor(Math.random() * (ceilMax - ceilMin)) + min;
}

export function sha256hex(data: Buffer) {
  return crypto.createHash('sha256').update(data).digest('hex');
}

/**
 * Used with functions "onCall" to return error in API endpoint.
 */
export function sendErr(res: any, what: string | Error, code: number) {
  let msg = '';
  if (what instanceof Error) {
    msg = `Exception: ${what}`;
  } else {
    msg = what;
  }
  logger().info(`Responding to client with error: ${msg} (code: ${code})`);
  return res.status(code).send(JSON.stringify({
    error: getReasonPhrase(code),
    message: msg,
  }));
}

export function isLowerCaseBitcoinCashAddress(str: string) {
  return /^bitcoincash:(q|p)[a-z0-9]{41}$/.test(str);
}

/**
 * Get a parameter from a POST request
 */
export function getPostParam<T>(req: any, param: string): T {
  const v: T = req.body[param];
  if (v === undefined) {
    throw new MissingParamError(param);
  }
  return v;
}

/**
 * Get a parameter from a POST request or default to `defaultValue`
 */
export function getPostParamOr<T>(
  req: any,
  param: string,
  defaultValue: T,
): T {
  const v: T = req.body[param];
  return v === undefined ? defaultValue : v;
}

/**
 * Get a parameter from a GET request
 */
export function getGetParam<T>(req: any, param: string): T {
  const v: T = req.query[param];
  if (v === undefined) {
    throw new MissingParamError(param);
  }
  return v;
}

/**
 * Get a parameter from a GET request or default to `defaultValue`
 */
export function getGetParamOr<T>(req: any, param: string, defaultValue: T): T {
  const v: T = req.query[param];
  return v === undefined ? defaultValue : v;
}
