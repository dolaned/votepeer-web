import { StatusCodes } from 'http-status-codes';
import {
  identify,
  requestChallenge,
  verifySignedChallenge,
} from './auth/auth_endpoint';
import { getTipHeight } from './blockchain/blockchain_endpoints';
import { getRegion } from './cfg';
import {
  getTally, createElection,
  getLinkedKeys, getElectionDetails,
  listPublicElections, listHostedElections, listParticipatingElections,
} from './election/election_endpoints';
import onNewElection from './election/on_new_election';
import { linkPublicKey, listUserAllowedTags } from './user/user_endpoint';
import { firebaseFunctions } from './db';
import { sendErr } from './util';
import {
  AuthenticationError,
  DoesNotExistError,
  InvalidParamError,
  MissingParamError,
  NoAccessError,
  ServerError,
  SignatureError,
  UserError,
} from './error';

const cors = require('cors')({ origin: true });

const region = getRegion();
interface HttpsEndpoint { onCall: any; onRequest: any }
const endpoint: HttpsEndpoint = firebaseFunctions().region(region).https;

/**
 * onRequest wrapper to handle cors, headers, exception, etc.
 */
async function wrapCall(
  // eslint-disable-next-line no-shadow
  req: any, res: any, call: (req: any, res: any) => Promise<string>,
) {
  res.setHeader('content-type', 'application/json');
  return cors(req, res, async () => {
    try {
      res.status(StatusCodes.OK).send(await call(req, res));
    } catch (e) {
      if (e instanceof MissingParamError
          || e instanceof InvalidParamError
          || e instanceof UserError) {
        return sendErr(res, e, StatusCodes.BAD_REQUEST);
      }
      if (e instanceof AuthenticationError
         || e instanceof NoAccessError) {
        return sendErr(res, e, StatusCodes.FORBIDDEN);
      }
      if (e instanceof SignatureError) {
        return sendErr(res, e, StatusCodes.IM_A_TEAPOT);
      }
      if (e instanceof ServerError) {
        return sendErr(res, e, StatusCodes.INTERNAL_SERVER_ERROR);
      }
      if (e instanceof DoesNotExistError) {
        return sendErr(res, e, StatusCodes.NOT_FOUND);
      }
      return sendErr(res, `Unknown error: ${e}`,
        StatusCodes.INTERNAL_SERVER_ERROR);
    }
    return res;
  });
}

/**
 * Auth
 */
exports.verifySignedChallenge = endpoint
  .onCall(verifySignedChallenge);

exports.request_challenge = endpoint
  .onCall(requestChallenge);

exports.identify = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, identify); });

/**
 * Elections
 */

exports.link_public_key = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, linkPublicKey); });

exports.create_election = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, createElection); });

exports.get_linked_keys = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, getLinkedKeys); });

exports.get_election_details = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, getElectionDetails); });

exports.list_public_elections = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, listPublicElections); });

exports.list_hosted_elections = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, listHostedElections); });

exports.list_participating_elections = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, listParticipatingElections); });

// ****
// Voting
// ****
exports.get_tally = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, getTally); });

/** *
 * Firebase messaging notification triggers
 ** */
exports.create_election_notification = firebaseFunctions().region(region)
  .firestore
  .document('/election/{electionId}')
  .onCreate(onNewElection);

/**
 * Blockchain queries
 */
exports.blockchain_tip_height = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, getTipHeight); });

/**
 * User
 */
exports.list_user_allowed_tags = endpoint
  .onRequest((req: any, res: any) => { wrapCall(req, res, listUserAllowedTags); });
