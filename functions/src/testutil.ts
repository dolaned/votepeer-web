import fs from 'fs';
import log from 'npmlog';
import path from 'path';
import { getProjectId } from './cfg';

// eslint-disable-next-line
export function hasFirebase(): boolean {
  const keyfile = `${getProjectId()}.json`;
  const keypath = path.join(__dirname, '..', 'keys', keyfile);
  log.info('db', keypath);
  return fs.existsSync(keypath);
}
