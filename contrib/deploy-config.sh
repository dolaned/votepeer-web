#!/bin/bash

## The firebase configuration needs to be published multiple places. This
## is a helper script to do so.

set -e
SCRIPT_PATH=`dirname $0`
CONFIG_PATH="./$SCRIPT_PATH/../votepeer.conf"

if [ ! -e $CONFIG_PATH ];
then
    echo "Config file $CONFIG_PATH does not exist."
    false
fi

source $CONFIG_PATH

# Validate config
[[ -z "$FIREBASE_API_KEY" ]] && { echo "FIREBASE_API_KEY is not set"; false; }

## ~/.firebaserc

FIREBASERC_PATH="./$SCRIPT_PATH/../.firebaserc"
echo "Writing to $FIREBASERC_PATH"
set +e
read -d '' FIREBASERPC_CONFIG << EndOfFirebaseRC
{
  "projects": {
    "production": "$FIREBASE_PROJECT_ID"
  }
}
EndOfFirebaseRC
set -e
echo "$FIREBASERPC_CONFIG" > $FIREBASERC_PATH

## ~/public
set +e
read -d '' VUE_CONFIG << EndOfVueConfig
VUE_APP_FIREBASE_API_KEY=$FIREBASE_API_KEY
VUE_APP_FIREBASE_APP_ID=$FIREBASE_APP_ID
VUE_APP_FIREBASE_AUTH_DOMAIN=$FIREBASE_AUTH_DOMAIN
VUE_APP_FIREBASE_DATABASE_URL=$FIREBASE_DATABASE_URL
VUE_APP_FIREBASE_SENDER_ID=$FIREBASE_SENDER_ID
VUE_APP_FIREBASE_PROJECT_ID=$FIREBASE_PROJECT_ID
VUE_APP_FIREBASE_STORAGE_BUCKET=$FIREBASE_STORAGE_BUCKET
VUE_APP_FIREBASE_REGION=$FIREBASE_REGION
EndOfVueConfig
set -e

[[ -z "$VUE_CONFIG" ]] && { echo "Failed to create vue config"; false; }

VUE_ENV_PATH="$SCRIPT_PATH/../public/.env"
VUE_ENV_PROD_PATH="$SCRIPT_PATH/../public/.env.production"

echo "Writing $VUE_ENV_PATH"
echo "Writing $VUE_ENV_PROD_PATH"
echo "$VUE_CONFIG" > $VUE_ENV_PATH
echo "$VUE_CONFIG" > $VUE_ENV_PROD_PATH


## ~/functions

# Local emulator
set +e
read -d '' RUNTIME_CONFIG << EndOfRuntimeConfig
{
  "votepeer": {
    "firebase_storage_bucket": "$FIREBASE_STORAGE_BUCKET",
    "firebase_api_key": "$FIREBASE_API_KEY",
    "firebase_auth_domain": "$FIREBASE_AUTH_DOMAIN",
    "firebase_region": "$FIREBASE_REGION",
    "firebase_database_url": "$FIREBASE_DATABASE_URL",
    "firebase_sender_id": "$FIREBASE_SENDER_ID",
    "firebase_project_id": "$FIREBASE_PROJECT_ID",
    "firebase_app_id": "$FIREBASE_APP_ID",
    "firebase_service_account_id": "$FIREBASE_SERVICE_ACCOUNT_ID"
  }
}
EndOfRuntimeConfig
set -e
RUNTIME_PATH="./$SCRIPT_PATH/../functions/.runtimeconfig.json"
echo "Writing to $RUNTIME_PATH"
echo "$RUNTIME_CONFIG" > $RUNTIME_PATH

# Firebase deployment
if [ -z ${SKIP_DEPLOY_FIRESTORE+x} ]; then
    echo "Updating firestore"
    (cd "./$SCRIPT_PATH/.."; npm run -s functions-config-set -- \
        "votepeer.firebase_api_key=$FIREBASE_API_KEY" \
        "votepeer.firebase_app_id=$FIREBASE_APP_ID" \
        "votepeer.firebase_auth_domain=$FIREBASE_AUTH_DOMAIN" \
        "votepeer.firebase_database_url=$FIREBASE_DATABASE_URL" \
        "votepeer.firebase_sender_id=$FIREBASE_SENDER_ID" \
        "votepeer.firebase_project_id=$FIREBASE_PROJECT_ID" \
        "votepeer.firebase_storage_bucket=$FIREBASE_STORAGE_BUCKET" \
        "votepeer.firebase_region=$FIREBASE_REGION" \
        "votepeer.firebase_service_account_id=$FIREBASE_SERVICE_ACCOUNT_ID"
    )
fi

