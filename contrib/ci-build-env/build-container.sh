set -e
# docker buildx create --name mybuilder --platform linux/amd64
docker buildx use mybuilder
docker buildx build --no-cache --platform linux/amd64 -t dagurval/votepeer-website-buildenv . --push