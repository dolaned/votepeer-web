import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import Alert from './alert';
import Main from './main';

Vue.use(Vuex);

const appModule: StoreOptions<any> = {
  modules: {
    Main,
    Alert,
  },
};
const Store = new Vuex.Store(appModule);

export default Store;
