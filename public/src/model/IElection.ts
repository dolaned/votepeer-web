import app from 'firebase/app';
import 'firebase/firestore';

export interface IElectionContent {
    readonly adminAddress: string;

    readonly participantAddresses: [string];
    readonly description: string;
    readonly salt: string;
    readonly createdAt: app.firestore.Timestamp;
    readonly modifiedAt: app.firestore.Timestamp;
    readonly endHeight: number;
    readonly electionType: string;

    // two-option-vote only
    readonly optionA: string;
    readonly optionB: string;

    // ringsig and multi-option.vote
    readonly beginHeight: number;
    readonly option: [string];
}

export interface IElection {
    readonly id: string;
    readonly content: IElectionContent;
}
